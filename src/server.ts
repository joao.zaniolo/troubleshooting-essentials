import express from 'express';
import { routes } from './infra/routes';

import './config/mongoose';
import './config/elastic';

const app = express();

app.use(express.json());
app.use(routes);

app.listen('3333', () => {
  console.info('🚀 Server running at http://localhost:3333');
});
