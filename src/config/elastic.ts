import elastic from 'elastic-apm-node';

elastic.start({
  // Override the service name from package.json
  // Allowed characters: a-z, A-Z, 0-9, -, _, and space
  serviceName: '',

  // Use if APM Server requires a secret token
  secretToken: 'If9QmlEBzBKaaeFxSK',

  // Set the custom APM Server URL (default: http://localhost:8200)
  serverUrl:
    'https://d3bb5884281f46279c9bd06c02733794.apm.us-west1.gcp.cloud.es.io:443',

  // Set the service environment
  environment: 'production',
});
