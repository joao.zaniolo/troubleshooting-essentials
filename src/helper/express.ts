import { Request, Response } from 'express';

export function expressHelper(callback: any) {
  return async function (req: Request, res: Response) {
    const { response, status = 200 } = await callback({
      params: req.params,
      body: req.body,
    });

    res.status(status).json(response);
  };
}
