import { Router } from 'express';
import { userRoutes } from '../core/User/routes';

const routes = Router();

routes.use('/users', userRoutes);

export { routes };
