import { Router } from 'express';
import { UserController } from './controller';
import { expressHelper } from '../../helper/express';

const userRoutes = Router();

userRoutes.post('/', expressHelper(UserController.create));
userRoutes.get('/', expressHelper(UserController.readAll));
userRoutes.get('/:id', expressHelper(UserController.readOne));
userRoutes.put('/:id', expressHelper(UserController.update));
userRoutes.delete('/:id', expressHelper(UserController.delete));

export { userRoutes };
