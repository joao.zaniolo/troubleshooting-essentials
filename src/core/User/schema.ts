import { Schema, model } from 'mongoose';

export const UserModel = model(
  'Users',
  new Schema(
    {
      email: {
        type: String,
        required: true,
      },
      password: {
        type: String,
        required: true,
        select: false,
      },
    },
    {
      timestamps: true,
    }
  )
);
