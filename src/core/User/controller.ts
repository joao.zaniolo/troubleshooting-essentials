import { UserModel } from './schema';

interface ControllerParams {
  params: {
    id: string;
  };
  body: {
    email: string;
    password: string;
  };
}

export class UserController {
  static async create({ body }: ControllerParams) {
    const { email, password } = body;

    try {
      return {
        response: await new UserModel({ email, password }).save(),
        status: 201,
      };
    } catch (error) {
      return { response: error.message, status: 400 };
    }
  }

  static async readAll() {
    try {
      return { response: await UserModel.find(), status: 200 };
    } catch (error) {
      return { response: error.message, status: 500 };
    }
  }

  static async readOne({ params }: ControllerParams) {
    try {
      const user = await UserModel.findById(params.id);

      if (!user) throw new Error('User not found');

      return { response: user, status: 200 };
    } catch (error) {
      return { response: error.message, status: 404 };
    }
  }

  static async update({ params, body }: ControllerParams) {
    try {
      const updatedUser = await UserModel.findByIdAndUpdate(params.id, body, {
        new: true,
      });

      if (!updatedUser) throw new Error('User not found');

      return { response: updatedUser, status: 200 };
    } catch (error) {
      return { response: error.message, status: 404 };
    }
  }

  static async delete({ params }: ControllerParams) {
    try {
      await UserModel.findByIdAndDelete(params.id);
      return { response: undefined, status: 200 };
    } catch (error) {
      return { response: error.message, status: 404 };
    }
  }
}
